
import 'package:flutterwithlaravel/controller/history_page_controller.dart';
import 'package:flutterwithlaravel/controller/homepage_controller.dart';
import 'package:flutterwithlaravel/data/repository/cart_repo.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../data/repository/history_repo.dart';
import '../data/repository/popular_product_repo.dart';
import '../data/repository/recommended_product_repo.dart';

class HomePageBinding extends Bindings{
  @override
  void dependencies()async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Get.lazyPut(()=>PopularProductRepo());
    Get.lazyPut(()=>RecommendedProductRepo());
    Get.put(CartRep(sp: sharedPreferences));
    Get.put(HistoryRepo(sharedPreferences));
    Get.put(HomePageController(popularProductRepo: Get.find(), recommendedProductRepo: Get.find()));
    Get.put(HistoryController(historyRepo: Get.find()));
  }
}