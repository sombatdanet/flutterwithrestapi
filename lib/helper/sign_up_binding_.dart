import 'package:flutterwithlaravel/controller/sign_up_controller.dart';
import 'package:flutterwithlaravel/data/repository/auth_repo_.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUpBinding extends Bindings{
  @override
  void dependencies() async {
    final SharedPreferences sp = await SharedPreferences.getInstance();
    Get.lazyPut(()=>AuthRepo(sp: sp));
    Get.put(SignUpController(authRepo: Get.find()));
  }
}