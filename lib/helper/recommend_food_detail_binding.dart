
import 'package:flutterwithlaravel/controller/food_detial_screen_controller.dart';
import 'package:get/get.dart';

import '../controller/cart_page_controller.dart';

class RecommendedFoodBinding extends Bindings{
  @override
   void dependencies()  {
    Get.put(FoodDetailController());
    Get.put(CartPageController(cartRepo: Get.find(), historyRepo: Get.find()));
  }
}