
import 'package:flutterwithlaravel/controller/homepage_controller.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../controller/cart_page_controller.dart';
import '../data/repository/cart_repo.dart';
import '../data/repository/history_repo.dart';

class CartPageBinding extends Bindings{
  @override
  void dependencies() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Get.lazyPut(()=>CartRep(sp: sharedPreferences));
    Get.lazyPut(()=>HistoryRepo(sharedPreferences));
    Get.put(HomePageController(popularProductRepo: Get.find(), recommendedProductRepo: Get.find()));
    Get.put(CartPageController(cartRepo: Get.find(), historyRepo: Get.find()),);
  }
}