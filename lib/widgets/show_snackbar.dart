import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../constants/colors.dart';

void showSnackBar({String title = "FOODS", required String message}) {
  Get.snackbar(
    "", "",
    titleText:  Text(
      title,
      style: const TextStyle(color:Colors.white , fontSize: 20,fontWeight: FontWeight.bold),
    ),
    messageText:  Text(
      message,
      style: const TextStyle(color: Colors.white,fontWeight: FontWeight.w500),
    ),
    duration: const Duration(seconds: 1),
    backgroundColor:AppColors.mainColor,
    //colorText: Color(0xFFFFC700),
  );
}
void showErrorSnackBar({bool isError = true,String title = "ERROR",required String message}){
  Get.snackbar(
    "", "",
    titleText:  Text(
      title,
      style: const TextStyle(color:Colors.white , fontSize: 20,fontWeight: FontWeight.bold),
    ),
    messageText:  Text(
      message,
      style: const TextStyle(color: Colors.white,fontWeight: FontWeight.w500),
    ),
    duration: const Duration(seconds: 1),
    backgroundColor:Colors.red,
    //colorText: Color(0xFFFFC700),
  );
}