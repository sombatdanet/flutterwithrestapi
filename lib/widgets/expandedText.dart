
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/widgets/small_text.dart';

import '../constants/colors.dart';

class ExpandedText extends StatefulWidget {
  final String text;
  final double size;
  const ExpandedText({super.key, required this.text,this.size= 14});
  @override
  State<ExpandedText> createState() => _ExpandedTextState();
}

class _ExpandedTextState extends State<ExpandedText> {
   late String firstHalf;
   late String secondHalf;
     bool hintText = true;
  final double heightText = Dimenson.screenHeight/ 6;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.text.length>heightText){
      firstHalf = widget.text.substring(0,heightText.toInt());
      secondHalf = widget.text.substring(heightText.toInt()+1,widget.text.length);
    }else{
      firstHalf = widget.text;
      secondHalf = "";
    }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: secondHalf.isEmpty ?SmallText(text: firstHalf,size: widget.size):Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SmallText(text: hintText?("$firstHalf..."):(firstHalf+secondHalf),size: widget.size,),
          InkWell(
            child: Text(hintText?"show more":"show less",style: TextStyle(color: AppColors.mainColor,fontSize: widget.size),),
            onTap: (){
              setState(() {
                hintText=!hintText;
              });
            },
          )
        ],
      )
    );
  }
}
