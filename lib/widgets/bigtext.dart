import 'package:flutter/material.dart';

import '../constants/colors.dart';


class BigText extends StatelessWidget {
   final String text ;
   final Color? color;
   final TextOverflow overflow;
   final double size;
   final FontWeight? fontWeight;
   const BigText({super.key,
     required this.text,
    this.color =  const Color (0xFF332d2b),
     this.size =20,
     this.overflow = TextOverflow.ellipsis,
     this.fontWeight = FontWeight.w500,
});
   @override
  Widget build(BuildContext context) {
    return Text(text
    ,style: TextStyle(
        overflow: overflow,
        color:color,
        fontSize: size,
        fontWeight: fontWeight,
      ),
    );
  }
}
class ContainerIcon extends StatelessWidget {
 final  IconData? icon;
 final  Color? color;
 final   double size;
 final Color? iconColor;
 final   Color? background;
 final VoidCallback? onTap;
 final double? padding;
 const ContainerIcon({super.key, required this.icon,this.color,this.size =22,this.iconColor = const Color(0xFF332d2b),this.background= const Color(0xFFf7f6f4),this.onTap,this.padding = 14});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 50,
        width: 50,
        padding:  EdgeInsets.all(padding!),
        decoration: BoxDecoration(
          color:background,
          borderRadius: BorderRadius.circular(30),
        ),
        child: Center(
          child:Icon(icon,color: iconColor,size: size
            ,),
        )
      ),
    );
  }
}

