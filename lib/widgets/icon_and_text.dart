

import 'package:flutter/cupertino.dart';
import 'package:flutterwithlaravel/constants/colors.dart';
import 'package:flutterwithlaravel/widgets/small_text.dart';

class IconAndText extends StatelessWidget {
  final IconData icon;
  final String text;
  final Color iconColor;
  final Color color;
  final double? size;
   const IconAndText({
     super.key, this.size=24,
    required this.icon,
    required this.text,
    required this.iconColor,
    required this.color});
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(icon,color: iconColor,size:size,),const SizedBox(width: 5,),
        SmallText(text: text,color: color,),
      ],
    );
  }
}