import 'package:flutter/material.dart';

import '../constants/colors.dart';


class AppTextField extends StatelessWidget {
  final TextEditingController textEditingController;
  final String hintText;
  final IconData iconTextField;
  const AppTextField( {super.key, required this.textEditingController,required this.hintText,required this.iconTextField});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16,),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: Colors.white,
          boxShadow:  [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              blurRadius: 10.0,
              offset: const Offset(-5, 10),
            ),
          ]
      ),
      child: TextField(
        controller: textEditingController,
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(vertical:16),
            focusedBorder:  OutlineInputBorder(
              borderRadius: BorderRadius.circular(16),
              borderSide: const BorderSide(width: 1.0,color: Colors.white),
            ),
            enabledBorder:  OutlineInputBorder(
                borderRadius: BorderRadius.circular(16),
                borderSide: const BorderSide(width: 1.0,color: Colors.white)
            ),
            filled: true,
            fillColor: Colors.white,
            hintText: hintText,
            prefixIcon: Icon(iconTextField,color:AppColors.mainColor,),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(16),
            )
        ),
      ),
    );
  }
}
