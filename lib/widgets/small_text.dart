import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SmallText extends StatelessWidget {
  final String text ;
  final Color? color;
  final double size;
  final TextOverflow overflow;
  final double height ;
  const SmallText({super.key,
    required this.text,
    this.color =  Colors.grey,
    this.size =12,
    this.overflow = TextOverflow.fade,
    this.height = 1.2,
  });
  @override
  Widget build(BuildContext context) {
    return Text(text
      ,style: TextStyle(
        color:color,
        fontSize: size,
        height: height,
        overflow: overflow,
      ),
    );
  }
}