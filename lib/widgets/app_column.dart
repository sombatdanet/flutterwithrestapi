import 'package:flutter/material.dart';

import '../constants/colors.dart';
import 'icon_and_text.dart';

class AppColumn extends StatefulWidget {
  const AppColumn({super.key,});

  @override
  State<AppColumn> createState() => _AppColumnState();
}

class _AppColumnState extends State<AppColumn> {
  @override
  Widget build(BuildContext context) {
    return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const IconAndText(
              icon: Icons.circle,
              text: 'Normal',
              color: Colors.grey,
              iconColor: Colors.yellow,
            ),
            IconAndText(
              icon: Icons.location_on_outlined,
              text: '1.7km',
              color: Colors.grey,
              iconColor: AppColors.mainColor,
            ),
            const IconAndText(
              icon: Icons.access_time_rounded,
              text: '32mn',
              color: Colors.grey,
              iconColor: Colors.red,
            ),
          ],
    );
  }
}
