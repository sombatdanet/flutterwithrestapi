import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/controller/cart_page_controller.dart';
import 'package:flutterwithlaravel/controller/homepage_controller.dart';
import 'package:get/get.dart';
import '../../constants/colors.dart';
import '../../constants/getx_constant/api_constant.dart';
import '../../helper_rout/routes.dart';
import '../../widgets/bigtext.dart';
import '../../widgets/small_text.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  late CartPageController cartPageController;
  late HomePageController homePageController;
  @override
  void initState() {
    cartPageController = Get.find<CartPageController>();
    homePageController = Get.find<HomePageController>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CartPageController>(builder: (cartController) {
      return SafeArea(
        child: Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: () {cartPageController.clearFromLocal();},
            backgroundColor: AppColors.mainColor,
            child: const Icon(Icons.delete,color: Colors.white,),
          ),
            body: Column(
              children: [
                // build 3 Icon
                Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ContainerIcon(
                        onTap: () {
                          Get.back();
                        },
                        icon: Icons.arrow_back,
                        iconColor: Colors.white,
                        background: AppColors.mainColor,
                      ),
                      SizedBox(
                        width: Dimenson.width10 * 3,
                      ),
                      ContainerIcon(
                        onTap: () => Get.offAllNamed(AppRouts.mainHomePageScreen),
                        icon: Icons.home_outlined,
                        iconColor: Colors.white,
                        background: AppColors.mainColor,
                      ),
                      ContainerIcon(
                        onTap: () => Get.toNamed(AppRouts.historyScreen),
                        icon: Icons.shopping_cart,
                        iconColor: Colors.white,
                        background: AppColors.mainColor,
                      ),
                    ],
                  ),
                ),
                if (cartController.getCartList.isEmpty)
                  Container()
                else
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      child: ListView.builder(
                        itemCount: cartController.getCartList.length,
                        itemBuilder: (context, index) {
                          var cartList = cartController.getCartList[index];
                          return Container(
                            margin: const EdgeInsets.symmetric(vertical: 10),
                            height: Dimenson.screenHeight / 6.5,
                            child: Row(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    cartPageController.goToRout(cartList);
                                  },
                                  child: Container(
                                    width: Dimenson.screenWidth / 3.5,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: NetworkImage(
                                            ApiConstants.baseUrl +
                                                ApiConstants.uploads +
                                                cartList.img),
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: BorderRadius.circular(20),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    margin:
                                        const EdgeInsets.symmetric(vertical: 5),
                                    color: Colors.white,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          // Title of the food
                                          BigText(text: cartList.name),
                                          SmallText(
                                            text: cartList.name,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          Row(
                                            children: [
                                              BigText(
                                                text: "\$${cartList.price}",
                                                color: Colors.redAccent,
                                              ),
                                              Container(
                                                height:
                                                    Dimenson.screenHeight / 14,
                                                width:
                                                    Dimenson.screenWidth / 3.5,
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    GestureDetector(
                                                        onTap: () =>
                                                            cartController.addFoodToCart(cartList.product, -1),
                                                        child: const Icon(
                                                            Icons.remove)),
                                                    BigText(
                                                        text: cartList.quantity
                                                            .toString()),
                                                    GestureDetector(
                                                        onTap: () => cartController.addFoodToCart(cartList.product, 1),
                                                        child: const Icon(
                                                            Icons.add)),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ),
              ],
            ),
            bottomNavigationBar: _buildBottom(cartController)),
      );
    });
  }

  Widget _buildBottom(CartPageController cartPageController) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildBottomContainer(cartPageController),
      ],
    );
  }

  Widget _buildBottomContainer(CartPageController cartPageController) {
    return Container(
      height: Dimenson.screenHeight / 8,
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.1),
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(20), topRight: Radius.circular(20)),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
                height: Dimenson.screenHeight / 14,
                width: Dimenson.screenWidth / 2.5,
                child: Center(child: _buildWhiteContainer(cartPageController))),
            GestureDetector(
              onTap: () {
                  cartPageController.addToHistory(cartPageController.getCartList);
              },
              child: Container(
                  height: Dimenson.screenHeight / 14,
                  width: Dimenson.screenWidth / 2.5,
                  decoration: BoxDecoration(
                    color: AppColors.mainColor,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: const Center(
                      child: BigText(
                    text: "Checkout",
                    color: Colors.white,
                    size: 15,
                  ))),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildWhiteContainer(
    CartPageController pageController,
  ) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Center(
          child: BigText(
        text: '\$${pageController.getTotalAmount()}',
      )),
    );
  }
}
