import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/controller/cart_page_controller.dart';
import 'package:flutterwithlaravel/page/home_page/page_view_homepage/page_view_homepage.dart';
import 'package:flutterwithlaravel/page/home_page/popular_food_list_view/recommended_food_list_view.dart';
import 'package:get/get.dart';

import '../../constants/colors.dart';
import '../../controller/homepage_controller.dart';
import '../../widgets/bigtext.dart';
import '../../widgets/small_text.dart';

class HomePageScreen extends StatefulWidget {
  const HomePageScreen({super.key});

  @override
  State<HomePageScreen> createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomePageScreen> {
  late HomePageController homePageController;
  @override
  void initState() {
    // TODO: implement initState
    homePageController = Get.find<HomePageController>();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: GetBuilder<HomePageController>(
            builder:(_)=> SingleChildScrollView(
                child: Column(
                  children: [
                    _buildHeaderPage(),
                    homePageController.loading.value
                        ? const CircularProgressIndicator()
                        : const PageViewHomePage(),
                    _buildTextPopularFood(),
                    homePageController.loadings.value
                        ? const CircularProgressIndicator()
                        : const RecommendFoodListView(),
                  ],
                ),
              ),
        ),
        ),
    );
  }
  Widget _buildHeaderPage() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BigText(
                text: "Cambodia",
                color: AppColors.mainColor,
              ),
              Row(
                children: [
                  const SmallText(text: "Phnom Penh"),
                  Icon(
                    Icons.arrow_drop_down,
                    color: AppColors.mainColor,
                  )
                ],
              ),
            ],
          ),
          ContainerIcon(
            icon: Icons.search,
            background: AppColors.mainColor,
            iconColor: Colors.white,
          )
        ],
      ),
    );
  }
  Widget _buildTextPopularFood() {
    return const Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          BigText(text: "Recommended"),
          SizedBox(
            width: 10,
          ),
          SmallText(
            text: ".",
            size: 24,
          ),
          SizedBox(
            width: 10,
          ),
          SmallText(text: "Food paring"),
        ],
      ),
    );
  }
}

