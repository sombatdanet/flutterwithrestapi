
import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/Model/product_model.dart';
import 'package:flutterwithlaravel/constants/getx_constant/api_constant.dart';
import 'package:flutterwithlaravel/controller/homepage_controller.dart';
import 'package:flutterwithlaravel/helper_rout/routes.dart';
import 'package:flutterwithlaravel/widgets/bigtext.dart';
import 'package:flutterwithlaravel/widgets/small_text.dart';
import 'package:get/get.dart';
import '../../../constants/colors.dart';
import '../../../widgets/icon_and_text.dart';

class RecommendFoodListView extends StatelessWidget {
  const RecommendFoodListView({super.key});

  @override
  Widget build(BuildContext context) {
    final HomePageController homePageController = Get.find<HomePageController>();
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: homePageController.listRecommendProducts.length,
      itemBuilder: (context, index) {
        final list = homePageController.listRecommendProducts[index];
        return _buildListView(context,list,index);
      },
    );

  }

  Widget _buildListView(context,Product recommendedListFood,int index) {
    final double height = MediaQuery.of(context).size.height;
    final double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: (){
        Get.toNamed(AppRouts.getRecommendedDetailScreen(index,"homepage"));
      },
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        height: MediaQuery.of(context).size.height / 6.5,
        child: Row(
          children: [
            Container(
              width: width / 3,
              decoration:  BoxDecoration(
                  color: Colors.white,
                image:  DecorationImage(
                  image: NetworkImage(ApiConstants.baseUrl+ApiConstants.uploads+recommendedListFood.img),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(20),
              ),
            ),
            Expanded(
                child: Container(
                  margin: const EdgeInsets.symmetric(vertical: 15),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(12),
                      topRight: Radius.circular(20),
                    )
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        // Title of the food
                         BigText(text: recommendedListFood.name),
                         SmallText(text:recommendedListFood.description,overflow: TextOverflow.ellipsis,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                             IconAndText(
                              size: Dimenson.screenWidth/20,
                                icon: Icons.circle,
                                text: "Normal",
                                iconColor: Colors.yellow,
                                color: Colors.grey),
                            IconAndText(
                                size: Dimenson.screenWidth/20,
                                icon: Icons.location_on,
                                text: "2.5 km",
                                iconColor: AppColors.mainColor,
                                color: Colors.grey),
                             IconAndText(
                                size: Dimenson.screenWidth/20,
                                icon: Icons.access_time,
                                text: "Normal",
                                iconColor: Colors.red,
                                color: Colors.grey),
                          ],
                        ),
                      ],
                    ),
                  ),
            ),)
          ],
        ),
      ),
    );
  }
}
