
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/Model/product_model.dart';
import 'package:flutterwithlaravel/constants/getx_constant/api_constant.dart';
import 'package:flutterwithlaravel/controller/homepage_controller.dart';
import 'package:flutterwithlaravel/helper_rout/routes.dart';
import 'package:flutterwithlaravel/widgets/app_column.dart';
import 'package:flutterwithlaravel/widgets/bigtext.dart';
import 'package:flutterwithlaravel/widgets/small_text.dart';
import 'package:get/get.dart';

import '../../../constants/colors.dart';

class PageViewHomePage extends StatefulWidget {
  const PageViewHomePage({super.key,});

  @override
  State<PageViewHomePage> createState() => _PageViewHomePageState();
}

class _PageViewHomePageState extends State<PageViewHomePage> {
  PageController pageController = PageController(viewportFraction: 0.85);
  late final HomePageController _homePageController = Get.find<HomePageController>();
  var _currentPageValue = 0.0;
  @override
  void initState() {
    pageController.addListener(() {
      setState(() {
        _currentPageValue = pageController.page!;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height / 2.6,
            child: PageView.builder(
              controller: pageController,
              scrollDirection: Axis.horizontal,
              itemCount: _homePageController.listPopularProducts.length,
              itemBuilder: (context, index) {
                final list = _homePageController.listPopularProducts[index];
                return _buildPage(context,list,index);
              },
            ),
          ),
          DotsIndicator(
            dotsCount: _homePageController.listPopularProducts.isEmpty?1 : _homePageController.listPopularProducts.length ,
            position: _currentPageValue,
            decorator: DotsDecorator(
              activeColor: AppColors.mainColor,
              size: const Size.square(9.0),
              activeSize: const Size(18.0, 9.0),
              activeShape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
            ),
          ),
        ],
    );
  }

  Widget _buildPage(BuildContext context,Product popularList,int index) {
    return GestureDetector(
      onTap: (){
          Get.toNamed(AppRouts.getPopularDetailScreen(index,"home"));
        },
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 10),
            height: MediaQuery.of(context).size.height / 3.5,
            decoration: BoxDecoration(
                image:  DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                    ApiConstants.baseUrl+ApiConstants.uploads+popularList.img,
                    )),
                borderRadius: BorderRadius.circular(30)),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              height: MediaQuery.of(context).size.height / 6.5,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      offset: const Offset(5, 0),
                      blurRadius: 5.0,
                      color: Colors.grey.withOpacity(0.1),
                    ),
                    BoxShadow(
                      offset: const Offset(0, 5),
                      blurRadius: 5.0,
                      color: Colors.grey.withOpacity(0.1),
                    ),
                    const BoxShadow(offset: Offset(-5, 0), color: Colors.white),
                  ],
              ),
              child: Container(child: _buildTitleAndStar(popularList)),
            ),
          )
        ],
      ),
    );
  }
  Widget _buildTitleAndStar(Product popularList){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
           BigText(text: popularList.name),
          Row(
            children: [
              Wrap(
                children: List.generate(
                    5,
                        (index) => Icon(
                      Icons.star,
                      color: AppColors.mainColor,
                      size: 18,
                    )),
              ),
              const SizedBox(
                width: 10,
              ),
              const SmallText(text: "4.5"),
              const SizedBox(
                width: 10,
              ),
              const SmallText(text: "128 comment")
            ],
          ),
          const AppColumn()
        ],
      ),
    );
  }
}
