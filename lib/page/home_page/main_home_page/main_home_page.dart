
import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/controller/homepage_controller.dart';
import 'package:flutterwithlaravel/helper_rout/routes.dart';
import 'package:flutterwithlaravel/page/acount_page/profile_screen.dart';
import 'package:flutterwithlaravel/page/history_screen/history_screen.dart';
import 'package:get/get.dart';

import '../../../constants/colors.dart';
import '../home_page.dart';

class MainHomePage extends StatelessWidget {
    MainHomePage({super.key});

  final List page = [
    const HomePageScreen(),
    const HistoryScreen(),
  ];

  final HomePageController homePageController = Get.find<HomePageController>();
  @override
  Widget build(BuildContext context) {
    return Obx(
      ()=> Scaffold(
        body: page[homePageController.selectIndex.value],
        bottomNavigationBar: _buildBottom(),
      ),
    );
  }

  Widget _buildBottom(){
    return _buildBottomNavigation();
  }

  final List<BottomModel>  bottomList = [
    BottomModel(
      icon: Icons.home,
    ),
    BottomModel(
      icon: Icons.archive,
    ),
  ];

  Widget _buildBottomNavigation() {
    return  BottomNavigationBar(
        backgroundColor: Colors.transparent,
        showSelectedLabels: false,
        elevation: 0,
        currentIndex: homePageController.selectIndex.value,
        selectedItemColor: AppColors.mainColor,
        unselectedItemColor: Colors.amber,
        showUnselectedLabels: false,
        selectedFontSize: 0.0,
        unselectedFontSize: 0.0,
        items: List.generate(bottomList.length, (index) {
          final list = bottomList[index];
          return BottomNavigationBarItem(
            backgroundColor: Colors.transparent,
            icon:Icon(list.icon),
            label: "",
          );
        }),
        onTap: (index){
          homePageController.tabBottom(index);
        },
    );
  }
}

class BottomModel{
  final IconData icon;
  BottomModel({required this.icon});
}
