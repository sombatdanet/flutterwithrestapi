import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/helper_rout/routes.dart';
import 'package:flutterwithlaravel/widgets/bigtext.dart';
import 'package:get/get.dart';

import '../../constants/colors.dart';
import '../../constants/image_constant.dart';
import '../../widgets/app_text_field.dart';

class SignInScreen extends StatelessWidget {
  const SignInScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final TextEditingController textEditingController = TextEditingController();
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Container(
              height: Dimenson.screenHeight/4,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(ImageConstant.logo),
                  )
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 10),
              width: double.maxFinite,
              child: const Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BigText(text: "Hello",size: 40,fontWeight: FontWeight.bold,),
                  BigText(text: "Sign in to your account",color: Colors.grey,size: 16,)
                ],
              ),
            ),
            SizedBox(height: Dimenson.height20),
            AppTextField(
              textEditingController: textEditingController,
              hintText: "Email",
              iconTextField: Icons.email,
            ),
            SizedBox(height: Dimenson.height20),
            AppTextField(
              textEditingController: textEditingController,
              hintText: "Password",
              iconTextField: Icons.lock_outline,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal:16,vertical: Dimenson.height20),
              width: double.maxFinite,
              decoration: BoxDecoration(
                  color: AppColors.mainColor,
                  borderRadius: BorderRadius.circular(12)
              ),
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: const Center(child: BigText(text: "Sign In",color: Colors.white,)),
            ),
             SizedBox(
              width: double.maxFinite,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const BigText(text: "Don\'t have an account?",size: 16,color: Colors.grey,),
                  GestureDetector(
                    onTap: (){
                      Get.toNamed(AppRouts.signUpScreen);
                    },
                      child: const BigText(text: "Sign Up",size: 16,fontWeight: FontWeight.bold,))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
