import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/constants/image_constant.dart';
import 'package:flutterwithlaravel/helper_rout/routes.dart';
import 'package:get/get.dart';



class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with TickerProviderStateMixin {


  late Animation<double> animation;
  late AnimationController animationController;
  @override
  void initState() {
    super.initState();
    animationController = AnimationController(vsync: this,duration: const Duration(seconds: 2))..forward();
    animation = CurvedAnimation(parent: animationController, curve: Curves.linear);
    Timer(
      const Duration(seconds: 4),
        ()=>Get.offAllNamed(AppRouts.mainHomePageScreen),
    );
  }
  @override
  dispose() {
    animationController.dispose(); // you need this
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ScaleTransition(
            scale: animation,
              child: Image.asset(ImageConstant.logo)),
        ],
      )
    );
  }
}
