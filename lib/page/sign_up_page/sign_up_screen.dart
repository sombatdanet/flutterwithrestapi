import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/controller/sign_up_controller.dart';
import 'package:flutterwithlaravel/helper_rout/routes.dart';
import 'package:flutterwithlaravel/widgets/app_text_field.dart';
import 'package:flutterwithlaravel/widgets/bigtext.dart';
import 'package:get/get.dart';

import '../../constants/colors.dart';
import '../../constants/image_constant.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({super.key});

  @override
  Widget build(BuildContext context) {
    SignUpController signUpController = Get.find<SignUpController>();
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: Dimenson.screenHeight/4,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(ImageConstant.logo),
                  )
                ),
              ),
              AppTextField(
                  textEditingController: signUpController.emailController,
                  hintText: "Email",
                  iconTextField: Icons.email,
              ),
              SizedBox(height: Dimenson.height20),
              AppTextField(
                textEditingController: signUpController.passwordController,
                hintText: "Password",
                iconTextField: Icons.lock_outline,
              ),
              SizedBox(height: Dimenson.height20),
              AppTextField(
                textEditingController: signUpController.nameController,
                hintText: "Name",
                iconTextField: Icons.person,
              ),
              SizedBox(height: Dimenson.height20),
              AppTextField(
                textEditingController: signUpController.phoneController,
                hintText: "Phone",
                iconTextField: Icons.call,
              ),
              GestureDetector(
                onTap: (){
                  signUpController.registration();
                },
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal:16,vertical: Dimenson.height20),
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                    color: AppColors.mainColor,
                    borderRadius: BorderRadius.circular(12)
                  ),
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: const Center(child: BigText(text: "Sign Up",color: Colors.white,)),
                ),
              ),
               SizedBox(
                width: double.maxFinite,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const BigText(text: "Have account already ?",size: 16,color: Colors.grey,),
                    GestureDetector(
                      onTap: (){
                        Get.toNamed(AppRouts.signInScreen);
                      },
                        child: const BigText(text: "Sign In",size: 16,fontWeight: FontWeight.bold,))
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
