
import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/widgets/bigtext.dart';

class AccountWidgets extends StatelessWidget {
  final IconData iconData;
  final String text;
  final Color color;
  const AccountWidgets({super.key, required this.iconData, required this.text, required this.color});

  @override
  Widget build(BuildContext context) {
    return  Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            offset: const Offset(5, 0),
            blurRadius: 5.0,
            color: Colors.grey.withOpacity(0.1),
          ),
          BoxShadow(
            offset: const Offset(0, 5),
            blurRadius: 5.0,
            color: Colors.grey.withOpacity(0.1),
          ),
          const BoxShadow(offset: Offset(-5, 0), color: Colors.white),
        ],
      ),
      child: Row(
        children: [
          ContainerIcon(icon:iconData,iconColor: color)
        ],
      ),
    );
  }
}
