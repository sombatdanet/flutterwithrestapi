import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/widgets/bigtext.dart';

import '../../constants/colors.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar:AppBar(
          centerTitle: true,
          backgroundColor: AppColors.mainColor,
          title: const BigText(text: "Pofile",color:Colors.white,),
        ),
      ),
    );
  }
}
