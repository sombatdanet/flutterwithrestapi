import 'dart:convert';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/constants/colors.dart';
import 'package:flutterwithlaravel/controller/cart_page_controller.dart';
import 'package:flutterwithlaravel/controller/history_page_controller.dart';
import 'package:flutterwithlaravel/helper_rout/routes.dart';
import 'package:flutterwithlaravel/widgets/bigtext.dart';
import 'package:flutterwithlaravel/widgets/small_text.dart';
import 'package:get/get.dart';

import '../../constants/getx_constant/api_constant.dart';
import '../../model/cart_model.dart';

class HistoryScreen extends StatefulWidget {
  const HistoryScreen({super.key});

  @override
  State<HistoryScreen> createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  var historyController = Get.find<HistoryController>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Get.put(CartPageController(cartRepo: Get.find(), historyRepo: Get.find()),);
  }
  @override
  Widget build(BuildContext context) {

    Map<String, int> cartItemsPreorder = {};

    for (int i = 0; i < historyController.getHistoryList.length; i++) {
      if (cartItemsPreorder
          .containsKey(historyController.getHistoryList[i].time)) {
        cartItemsPreorder.update(
            historyController.getHistoryList[i].time, (value) => ++value);
      } else {
        cartItemsPreorder.putIfAbsent(
            historyController.getHistoryList[i].time, () => 1);
      }
    }

    List<int> listToInt() {
      return cartItemsPreorder.entries.map((e) => e.value).toList();
    }

    List<int> numberOfList = listToInt();

    List<String> timePerOrder(){
      return cartItemsPreorder.entries.map((e) => e.key).toList();
    }

    return GetBuilder<HistoryController>(
      builder: (_) => SafeArea(
        child: Scaffold(
          floatingActionButton: FloatingActionButton(
            backgroundColor: AppColors.mainColor,
              onPressed: () {historyController.clearFromHistory();},child: const Icon(Icons.delete,color: Colors.white,),),
            appBar: _buildAppBar(),
            body: _buildBody(historyController, numberOfList,timePerOrder())),
      ),
    );
  }
  AppBar _buildAppBar(){
    return  AppBar(
      leading: IconButton(onPressed: (){Get.back();},icon: const Icon(Icons.arrow_back,color: Colors.white,),),
      backgroundColor: AppColors.mainColor,
      title: const BigText(text: "History",color: Colors.white,),
      centerTitle: true,
    );
  }

  Widget _buildBody(HistoryController historyController, var numBer,var timePerOrder) {
    var listCount = 0;
    return Column(
      children: [
        historyController.getHistoryList.isNotEmpty?Expanded(child: ListView(
          children: [
            for (int i = 0; i < numBer.length; i++)
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 10,),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BigText(text: historyController.getHistoryList[i].formatDate().toString()),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                      Wrap(children: List.generate(numBer[i], (index) {print(numBer[i].toString());
                          if (listCount < historyController.getHistoryList.length) {listCount++;}
                          return index<=2?Container(
                            margin:  EdgeInsets.only(right: Dimenson.width10/2.5),
                            height: Dimenson.screenHeight / 10,
                            width: Dimenson.screenWidth / 5,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(Dimenson.width10),
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(ApiConstants.baseUrl + ApiConstants.uploads + historyController.getHistoryList[listCount-1].img),
                                )),
                          ):Container();
                        })),
                      GestureDetector(
                        onTap: (){
                          var moreOrder = timePerOrder;
                          Map<int,CartModel> seeMore = {};
                          final list  = historyController.getHistoryList;
                          for(int x =0;x< list.length ;x++){
                            if(list[x].time == moreOrder[i]){
                              seeMore.putIfAbsent(list[x].id, () => CartModel.fromJson(jsonDecode(jsonEncode(list[x]))));
                            }
                          }
                          Get.find<CartPageController>().setItemHistoryToCart = seeMore;
                          Get.find<CartPageController>().addHistoryListToCart();
                          Get.toNamed(AppRouts.cartScreen);
                        },
                        child: SizedBox(
                          height: Dimenson.screenHeight / 8,
                          width: Dimenson.screenWidth / 4,
                          child: Column(
                            children: [
                              const SmallText(text: "Total"),
                              BigText(text: "${numBer[i]} Items",size: 20,),
                              Container(
                                decoration: BoxDecoration(
                                  border: Border.all(width: 1,color:AppColors.mainColor),
                                  borderRadius: BorderRadius.circular(5)
                                ),
                                child: Padding(
                                  padding:  const EdgeInsets.all(5),
                                  child: SmallText(text: "one more",color: AppColors.mainColor,size: 15,),
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                      ],
                    )
                  ],
                ),
              )
          ],
        )):Container(),
      ],
    );
  }
}
