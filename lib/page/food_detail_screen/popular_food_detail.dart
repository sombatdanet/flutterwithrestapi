import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../Model/product_model.dart';
import '../../constants/colors.dart';
import '../../constants/getx_constant/api_constant.dart';
import '../../controller/cart_page_controller.dart';
import '../../controller/food_detial_screen_controller.dart';
import '../../controller/homepage_controller.dart';
import '../../helper_rout/routes.dart';
import '../../widgets/bigtext.dart';
import '../../widgets/expandedText.dart';
import '../../widgets/small_text.dart';

class PopularFoodDetail extends StatefulWidget {
  final int index;
  final String page;
  const PopularFoodDetail({super.key, required this.index,required this.page});

  @override
  State<PopularFoodDetail> createState() => _PopularFoodDetailState();
}

class _PopularFoodDetailState extends State<PopularFoodDetail> {
  late Product popularFoodList;
  var foodDetailControllers = Get.find<FoodDetailController>().intiProduct(Get.find<CartPageController>());
  @override
  void initState() {
    super.initState();
    popularFoodList = Get.find<HomePageController>().listPopularProducts[widget.index];
  }
  late FoodDetailController foodDetailController= Get.find();
  Widget _buildSliverAppBar(Product popularFoodList ) {
    return GetBuilder<CartPageController>(
      builder: (_)=>SliverAppBar(
        automaticallyImplyLeading: false,
        toolbarHeight: 100,
        title:  Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ContainerIcon(
              onTap: (){
                if(widget.page == 'cartPage'){
                  Get.toNamed(AppRouts.cartScreen);
                }else{
                  Get.toNamed(AppRouts.mainHomePageScreen);
                }
              },
              icon: Icons.clear,
            ),
            Stack(
              children: [
                ContainerIcon(
                  onTap: (){Get.toNamed(AppRouts.cartScreen);},
                  icon: Icons.shopping_cart_outlined,
                ),
                Positioned(
                    right: 0,
                    child: foodDetailController.getQty()>1?Container(
                      height: Dimenson.height20,
                      width: Dimenson.width20,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColors.mainColor,
                      ),
                      child: Center(
                        child: SmallText(text: foodDetailController.getQty().toString(),color: Colors.white,),
                      ),
                    ):Container(
                      height: Dimenson.height20,
                      width: Dimenson.width20,
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.transparent
                      ),
                    )
                )
              ],
            ),
          ],
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(20),
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 5),
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20), topLeft: Radius.circular(20))),
            child:  Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 14),
                  child: BigText(
                    text: popularFoodList.name,
                    size: 24,
                  ),
                )),
          ),
        ),
        backgroundColor: Colors.yellow,
        expandedHeight: 300,
        pinned: true,
        flexibleSpace: FlexibleSpaceBar(
          background:Image.network(
            ApiConstants.baseUrl+ApiConstants.uploads+popularFoodList.img,
            fit: BoxFit.cover,
            width: double.maxFinite,
          ),
        ),
      ),
    );
  }

  Widget _buildBottom(Product popularFoodList) {
    return GetBuilder<FoodDetailController>(
      builder: (_)=> Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ContainerIcon(
                  padding: 10,
                  onTap: (){
                    foodDetailController.increase(false);
                  },
                  icon: Icons.remove,
                  iconColor: Colors.white,
                  background: AppColors.mainColor,
                  size: 20,
                ),
                BigText(
                  text: '\$ ${popularFoodList.price}.00 X ${foodDetailController.quantity}',
                  size: 24,
                ),
                ContainerIcon(
                    padding: 10,
                    onTap: (){
                      foodDetailController.increase(true);
                    },
                    icon: Icons.add,
                    iconColor: Colors.white,
                    background: AppColors.mainColor,
                    size: 20),
              ],
            ),
          ),
          _buildBottomContainer(popularFoodList),
        ],
      ),
    );
  }

  Widget _buildBottomContainer(Product popularFoodList) {
    return Container(
      height: Dimenson.screenHeight / 8,
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.1),
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(20), topRight: Radius.circular(20)),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
                height: 70,
                width: 70,
                child: Center(child: _buildWhiteContainer(popularFoodList))),
            GestureDetector(
              onTap: () => foodDetailController.addToCart(popularFoodList),
              child: Container(
                  height: Dimenson.screenHeight / 14,
                  width: Dimenson.screenWidth / 2.5,
                  decoration: BoxDecoration(
                    color: AppColors.mainColor,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: const Center(
                      child: BigText(
                        text: "\$10 | Add to cart",
                        color: Colors.white,
                        size: 15,
                      ))
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildWhiteContainer(Product popularFoodList,) {
    return  Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Center(
          child:
          Icon(Icons.favorite,color:AppColors.mainColor,)
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            _buildSliverAppBar(popularFoodList,),
            SliverToBoxAdapter(
              child: Column(
                children: [
                  Container(
                    margin:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    child:  ExpandedText(
                      size: 18,
                      text:
                      popularFoodList.description,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
        bottomNavigationBar: _buildBottom(popularFoodList),
      ),
    );
  }
}
