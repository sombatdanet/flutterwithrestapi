import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/controller/homepage_controller.dart';
import 'package:flutterwithlaravel/helper_rout/routes.dart';
import 'package:flutterwithlaravel/widgets/bigtext.dart';
import 'package:flutterwithlaravel/widgets/expandedText.dart';
import 'package:get/get.dart';

import '../../Model/product_model.dart';
import '../../constants/colors.dart';
import '../../constants/getx_constant/api_constant.dart';
import '../../controller/cart_page_controller.dart';
import '../../controller/food_detial_screen_controller.dart';
import '../../widgets/icon_and_text.dart';
import '../../widgets/small_text.dart';

class RecommendedFoodDetail extends StatefulWidget {
  final int index;
  final String page;
  const RecommendedFoodDetail({super.key,required this.index, required this.page});

  @override
  State<RecommendedFoodDetail> createState() => _RecommendedFoodDetailState();
}

class _RecommendedFoodDetailState extends State<RecommendedFoodDetail> {
  late Product recommendedFoodList;
  late FoodDetailController foodDetailController;
  var foodDetailControllers = Get.find<FoodDetailController>().intiProduct(Get.find<CartPageController>());
  @override
  void initState() {
    super.initState();
    recommendedFoodList = Get.find<HomePageController>().listRecommendProducts[widget.index];
    foodDetailController = Get.find<FoodDetailController>();
  }
  @override
  Widget build(BuildContext context) {
    FoodDetailController controller = Get.find<FoodDetailController>();
    return GetBuilder<CartPageController>(
        builder:(_)=> SafeArea(
          child: Scaffold(
            body: Stack(
              children: [
                Positioned(
                  left: 0,
                  right: 0,
                  child: Container(
                    height: Dimenson.screenHeight / 2.5,
                    decoration:  BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(ApiConstants.baseUrl+ApiConstants.uploads+recommendedFoodList.img),
                            fit: BoxFit.cover)),
                  ),
                ),
                Positioned(
                  top: 20,
                  right: 20,
                  left: 20,
                  child:  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ContainerIcon(
                        onTap: ()=>Get.back(),
                        icon: Icons.arrow_back,
                      ),
                      Stack(
                        children: [
                          ContainerIcon(
                            onTap: (){
                              Get.toNamed(AppRouts.cartScreen);
                            },
                            icon: Icons.shopping_cart_outlined,
                          ),
                          Positioned(
                              right: 0,
                              child: foodDetailController.getQty()>1?Container(
                                height: Dimenson.height20,
                                width: Dimenson.width20,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: AppColors.mainColor,
                                ),
                                child: Center(
                                  child: SmallText(text: foodDetailController.getQty().toString(),color: Colors.white,),
                                ),
                              ):Container(
                                height: Dimenson.height20,
                                width: Dimenson.width20,
                                decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.transparent
                                ),
                              )
                          )
                        ],
                      ),
                    ],
                  ),),
                _buildIntroducePage(Dimenson.screenHeight, recommendedFoodList,controller),
              ],
            ),
            bottomNavigationBar: _buildBottomContainer(Dimenson.screenHeight, Dimenson.screenWidth,recommendedFoodList,controller),
          ),
        ),
    );
  }

  Widget _buildIntroducePage(double height, Product recommendedFoodList,FoodDetailController controller) {
    return Positioned(
      top: height / 2.6,
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(20), topLeft: Radius.circular(20)),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  margin: const EdgeInsets.symmetric(vertical: 5),
                  height: height / 7,
                  child: _buildTitleAndStar(recommendedFoodList,controller)),
              const Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: BigText(text: "Introduce")),
               Expanded(
                child: SingleChildScrollView(
                  child: ExpandedText(
                      size: 18,
                      text:recommendedFoodList.description),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTitleAndStar(Product recommendedFoodList,FoodDetailController controller) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
         BigText(text: recommendedFoodList.name),
        Row(
          children: [
            Wrap(
              children: List.generate(
                  5,
                  (index) => Icon(
                        Icons.star,
                        color: AppColors.mainColor,
                        size: 18,
                      )),
            ),
            const SizedBox(
              width: 10,
            ),
            const SmallText(text: "4.5"),
            const SizedBox(
              width: 10,
            ),
            const SmallText(text: "128 comment")
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const IconAndText(
                icon: Icons.circle,
                text: "Normal",
                iconColor: Colors.yellow,
                color: Colors.grey),
            IconAndText(
                icon: Icons.location_on,
                text: "2.5 km",
                iconColor: AppColors.mainColor,
                color: Colors.grey),
            const IconAndText(
                icon: Icons.access_time,
                text: "32 min",
                iconColor: Colors.red,
                color: Colors.grey),
          ],
        ),
      ],
    );
  }

  Widget _buildBottomContainer(double height, double width,Product recommendedFoodList,FoodDetailController controller) {
    return Container(
      height: height / 8,
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.1),
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(20), topRight: Radius.circular(20)),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildWhiteContainer(controller),
            GestureDetector(
              onTap: () => controller.addToCart(recommendedFoodList),
              child: Container(
                height: height / 14,
                width: width / 2.5,
                decoration: BoxDecoration(
                  color: AppColors.mainColor,
                  borderRadius: BorderRadius.circular(20),
                ),
                child:Center(
                    child: BigText(
                      text: "\$${recommendedFoodList.price.toString()} | Add to cart",
                      color: Colors.white,
                      size: 15,
                    )),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildWhiteContainer(FoodDetailController controller) {
    return  GetBuilder<FoodDetailController>(
      builder:(_)=> Container(
        height: Dimenson.screenHeight / 14,
        width: Dimenson.screenWidth / 3.5,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
                onTap: () => controller.increase(false),
                child: const Icon(Icons.remove)),
            BigText(text: controller.quantity.toString()),
            GestureDetector(
                onTap: () => controller.increase(true),
                child: const Icon(Icons.add)),
          ],
        ),
      ),
    );
  }
}
