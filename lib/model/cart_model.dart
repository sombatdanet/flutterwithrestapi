import 'package:flutterwithlaravel/Model/product_model.dart';
import 'package:intl/intl.dart';

class CartModel {
  final int id;
  final String name;
  final int price;
  final String img;
  int quantity;
   String time;
  final bool isExist;
   Product product;

  CartModel({
    required this.id,
    required this.name,
    required this.price,
    required this.img,
    this.quantity =1,
    required this.time,
    required this.isExist,
    required this.product,
  });

  factory CartModel.fromJson(Map<String, dynamic> json) {
    return CartModel(
      id: json['id'],
      name: json['name'],
      price: json['price'],
      img: json['img'],
      quantity: json['quantity'],
      time: json['time'],
      isExist: json['isExist'],
      product: Product.fromJson(json['product']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'price': price,
      'img': img,
      'quantity': quantity,
      'time': time,
      'isExist': isExist,
      'product': product.toJson(),
    };
  }
  String formatDate() => DateFormat("yyyy/MM/dd hh:mm a").format(DateTime.parse(time));
}

