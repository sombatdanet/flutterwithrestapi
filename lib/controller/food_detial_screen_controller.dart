
import 'package:flutterwithlaravel/Model/product_model.dart';
import 'package:flutterwithlaravel/controller/cart_page_controller.dart';
import 'package:get/get.dart';
class FoodDetailController extends GetxController{

   int _quantity = 0;
   int get quantity => _quantity;
   late  CartPageController _cartPageController = Get.find();
   void increase(bool isIncrease){
    if(isIncrease) {
      _quantity = checkQuantity(_quantity+1);
    } else {
      _quantity = checkQuantity(_quantity-1);
    }
    update();
  }

  int checkQuantity(int qty) {
    if (qty < 0) {
      return 0;
    }else if (qty > 20) {
      return  20;
    }else{
      return qty;
    }
  }
  void intiProduct (CartPageController cartPageController){
    _cartPageController = cartPageController;
    _quantity = 0;
  }

  void addToCart(Product product){
    _cartPageController.addFoodToCart(product,quantity);
    _quantity = 0 ;
    update();
  }
  int  getQty(){
   return  _cartPageController.getTotalQuantity();
  }
}