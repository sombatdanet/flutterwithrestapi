import 'package:flutter/cupertino.dart';
import 'package:flutterwithlaravel/data/repository/auth_repo_.dart';
import 'package:flutterwithlaravel/model/respone_model.dart';
import 'package:flutterwithlaravel/model/sig_up_model.dart';
import 'package:flutterwithlaravel/widgets/show_snackbar.dart';
import 'package:get/get.dart';

class SignUpController extends GetxController{
  final AuthRepo authRepo;
  SignUpController({required this.authRepo});

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var phoneController = TextEditingController();
  var passwordController = TextEditingController();
  SignUpModel signUpModel  = SignUpModel(name: "", phone: "", email: "", password: "");
  checkValidator(){
    String name = nameController.text.trim();
    String phone = phoneController.text.trim();
    String email = emailController.text.trim();
    String password = passwordController.text.trim();
    if(name.isEmpty){
      showErrorSnackBar(title: "Name",message: "Type your name");
    }else if(email.isEmpty){
      showErrorSnackBar(title: "Email",message: "Type your email");
    }else if(GetUtils.isEmail(email)){
      showErrorSnackBar(title: "Email",message: "Type valid your email");
    }else if(phone.isEmpty){
      showErrorSnackBar(title: "Phone",message: "Type your Phone Number");
    }else if(password.isEmpty){
      showErrorSnackBar(title: "Password",message: "Type your Password");
    }else if(password.length >6){
      showErrorSnackBar(title: "Password",message: "Password must greater than 6 character");
    }else{
       signUpModel = SignUpModel(name: name, phone: phone, email: email, password: password);
    }
  }
  Future<ResponseModel> registration() async {
    _isLoading = true;
    final res = await authRepo.registration(signUpModel);
    late ResponseModel responseModel;
    if(res.statusCode == 200){
      authRepo.saveUserToken(res.data['token']);
      responseModel = ResponseModel(true, res.data['token']);
      print(responseModel.message);
    }else{
      responseModel = ResponseModel(false, res.statusMessage!);
    }
    _isLoading = true;
    update();
    return responseModel;
  }
}