
import 'package:dio/dio.dart';
import 'package:flutterwithlaravel/Model/product_model.dart';
import 'package:flutterwithlaravel/data/repository/popular_product_repo.dart';
import 'package:flutterwithlaravel/data/repository/recommended_product_repo.dart';
import 'package:get/get.dart';

class HomePageController extends GetxController{

  final PopularProductRepo popularProductRepo;
  final  RecommendedProductRepo recommendedProductRepo;

  Rx<bool> loading = false.obs;
  Rx<bool> loadings = false.obs;
  Rx<String> errorMsg = "".obs;

  List<Product> listPopularProducts = RxList.empty(growable: true);

  List<Product> listRecommendProducts = RxList.empty(growable: true);

  HomePageController({required this.popularProductRepo,required this.recommendedProductRepo});
  @override
  void onInit() async{
    Future.delayed(Duration.zero, () {
      getPopularFoodProduct();
      getRecommendedFoodProduct();
    });
    super.onInit();
  }
  Future<void> getPopularFoodProduct() async {
    update();
    try{
      loading.value = true;
      final res = await popularProductRepo.getPopularProductList();
      listPopularProducts = res;
      loading(false);
    }on DioException catch(e){
      errorMsg(e.message ?? "Opp! Something went wrong");
    }
  }
  Future<void> getRecommendedFoodProduct() async {
    update();
    try{
      loadings(true);
      final response = await recommendedProductRepo.getRecommendedProductList();
      listRecommendProducts = response;
      loadings(false);
    }on DioException catch(e){
      errorMsg(e.message ?? "Opp! Something went wrong");
    }
  }
  Rx<int> selectIndex = 0.obs;
  int tabBottom(int index){
   return  selectIndex.value = index;
  }
}