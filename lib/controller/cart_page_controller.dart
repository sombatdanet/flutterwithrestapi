
import 'package:flutterwithlaravel/Model/product_model.dart';
import 'package:flutterwithlaravel/controller/homepage_controller.dart';
import 'package:flutterwithlaravel/data/repository/cart_repo.dart';
import 'package:flutterwithlaravel/data/repository/history_repo.dart';
import 'package:flutterwithlaravel/widgets/show_snackbar.dart';
import 'package:get/get.dart';

import '../helper_rout/routes.dart';
import '../model/cart_model.dart';

class CartPageController extends GetxController {


  final CartRep cartRepo;
  final HistoryRepo historyRepo;
  CartPageController({required this.cartRepo, required this.historyRepo});
  // this list use for add to cart
   HomePageController homePageController= Get.find();
  Map<int,CartModel> get itemCart => _itemCart;
   Map<int,CartModel> _itemCart = {};
  List<CartModel> get storageCartList => _storageCartList;
  List<CartModel> _storageCartList = [];
  // this list use for add cart to history

  @override
  void onInit(){
    getData();
    getTotalQuantity();
    getCartList;
    super.onInit();
  }
  void addFoodToCart(Product product, int qty) {
    var totalQty = 0;
    if (_itemCart.containsKey(product.id)) {
      _itemCart.update(product.id, (value) {
        totalQty = value.quantity+qty;
        return CartModel(
          id: product.id,
          name: product.name,
          price: product.price,
          img: product.img,
          quantity: value.quantity + qty,
          time: DateTime.now().toString(),
          isExist: true,
          product: product,
        );
      });
      if (totalQty <= 0) {
        _itemCart.remove(product.id);
      }
    } else {
      if(qty > 0){
        _itemCart.putIfAbsent(product.id, () {
            return CartModel(
              id: product.id,
              name: product.name,
              price: product.price,
              img: product.img,
              quantity: qty,
              time: DateTime.now().toString(),
              isExist: true,
              product: product,
            );
          },
        );
      }
    }
    cartRepo.addToCarts(getCartList);
    update();
  }
  List<CartModel> get getCartList{
    return _itemCart.entries.map((e) => e.value).toList();
  }
  int  getTotalQuantity(){
    var  totalQty = 0.obs;
    _itemCart.forEach((key, value) {
      totalQty.value +=value.quantity;
    });
    return totalQty.value;
  }
  int getTotalAmount(){
    int totalAmount = 0;
    _itemCart.forEach((key, value) {
      totalAmount +=value.quantity * value.price;
    });
    return totalAmount;
  }

  List<CartModel> getData(){
    updateItem();
    setCartList = (cartRepo.getLocalListCartModel());
    return _storageCartList;
  }
  set setCartList(List<CartModel> list){
    _storageCartList = list;
    for(int i =0; i<_storageCartList.length ;i++){
      _itemCart.putIfAbsent(_storageCartList[i].id, () => _storageCartList[i]);
    }
  }
  void clearFromLocal(){
    updateItem();
    _itemCart.clear();
    cartRepo.removeCartLocal();
  }
  void updateItem(){
    _itemCart;
    update();
  }
  // add cart to list
  void addToHistory(List<CartModel> list)async{
   final isAdd = await historyRepo.addHistory(list);
    if(isAdd){
      clearFromLocal();
    }
  }
  set setItemHistoryToCart(Map<int,CartModel> mapHistory){
    _itemCart={};
    _itemCart = mapHistory;
  }
  void addHistoryListToCart(){
    cartRepo.addToCarts(getCartList);
  }
  // help rout go to detail screen

  void goToRout(CartModel cartList){
    var recommendedIndex = homePageController.listRecommendProducts.indexOf(cartList.product);
    if (recommendedIndex>=0) {
      print("recommend index is $recommendedIndex");
      Get.toNamed(AppRouts.getRecommendedDetailScreen(recommendedIndex, "cartPage"));
    } else {
      var  popularProductIndex = homePageController.listPopularProducts.indexOf(cartList.product);
      if(popularProductIndex<0){
        showSnackBar(message: "This Product is not available");
      }else{
        Get.toNamed(AppRouts.getPopularDetailScreen(popularProductIndex, "cartPage"));
      }
    }
  }
}
