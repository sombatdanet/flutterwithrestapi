
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import '../data/repository/history_repo.dart';
import '../model/cart_model.dart';

class HistoryController extends GetxController{
  final HistoryRepo historyRepo;
  HistoryController({required this.historyRepo});
  List<CartModel> listHistory = [];
  List<CartModel> listOfHistory = [];
  @override
  void onReady() async{
    getList();
    super.onReady();
  }
  void clearFromHistory(){
    update();
    historyRepo.clear();
    listHistory.clear();
  }
  List<CartModel>  getList(){
    update();
    listOfHistory = listHistory;
    return listOfHistory;
  }
   List<CartModel> get getHistoryList{
   final list =  historyRepo.getHistory();
   return list;
  }
}