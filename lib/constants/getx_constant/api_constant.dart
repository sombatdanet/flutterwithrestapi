class ApiConstants{
  static const  String baseUrl = "http://mvs.bslmeiyu.com";
  static const String popularPath = "/api/v1/products/popular";
  static const String recommendedPath = "/api/v1/products/recommended";
  static const String uploads = "/uploads/";
  static  String token = "DBtoken";

  // auth constant
  static const String authPath = '/api/v1/auth/register';

  // key for local
}