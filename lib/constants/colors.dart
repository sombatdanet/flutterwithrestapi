import 'dart:ui';

import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

class AppColors {
  static final Color textColor = const Color (0xFFccc7c5);
  static final Color mainColor = const Color (0xFF89dad0);
//static final Color mainColor = const Color (0xFFfa7552);
  static final Color iconColor1 = const Color (0xFFFfd28d);
  static final Color iconColor2 = const Color (0xFFfcab88);
  static final Color paraColor = const Color (0xFF8f837f);
  static final Color buttonBackgroundColor= const Color (0xFFf7f6f4);
  static final Color signColor = const Color (0xFFa9a29f);
  static final Color titleColor = const Color (0xFF5c524f);
  static final Color mainBlackColor = const Color (0xFF332d2b);
//static final Color yellowColor = const Color (0xFFfa7552);
  static final Color yellowColor = const Color (0xFFffd379);
}
class Dimenson{
  static double screenHeight = Get.context!.height;
  static double screenWidth = Get.context!.width;

  static double heightPageView = screenHeight /3.415;
  static double height900 = screenHeight /0.7588;
  static double heightTextContainer = screenHeight/5.691;
  static double heightRaduis = screenHeight /34.15;

  static double height40  = screenHeight /17.075;
  static double width40  = screenWidth /17.075;
  static double height20 = screenHeight /34.15;
  static double width20 = screenHeight /34.15;
  static double width10 = screenHeight /68.3;
  static double height100 = screenHeight/6.83;
  static double width100 = screenWidth/6.83;
  static double height300 = screenHeight/2.27;

  static double iconSize = screenHeight/15;
}
