import 'package:flutter/material.dart';
import 'package:flutterwithlaravel/helper/home_page_binding.dart';
import 'package:flutterwithlaravel/helper/sign_up_binding_.dart';
import 'package:flutterwithlaravel/helper_rout/routes.dart';
import 'package:get/get.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialBinding: HomePageBinding(),
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple)
            .copyWith(background: Colors.grey[100]),
      ),
      initialRoute: AppRouts.splashscreen,
      getPages: AppRouts.pages,
    );
  }
}
