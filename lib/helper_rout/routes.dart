
import 'package:flutterwithlaravel/helper/cart_page_binding.dart';
import 'package:flutterwithlaravel/helper/home_page_binding.dart';
import 'package:flutterwithlaravel/helper/popular_food_detail_binding.dart';
import 'package:flutterwithlaravel/helper/recommend_food_detail_binding.dart';
import 'package:flutterwithlaravel/helper/sign_up_binding_.dart';
import 'package:flutterwithlaravel/page/cart_page/cart_screen.dart';
import 'package:flutterwithlaravel/page/food_detail_screen/popular_food_detail.dart';
import 'package:flutterwithlaravel/page/food_detail_screen/recommended_food_detail_screen.dart';
import 'package:flutterwithlaravel/page/history_screen/history_screen.dart';
import 'package:flutterwithlaravel/page/home_page/home_page.dart';
import 'package:flutterwithlaravel/page/home_page/main_home_page/main_home_page.dart';
import 'package:flutterwithlaravel/page/sign_in_screen/sign_in_screen.dart';
import 'package:flutterwithlaravel/page/sign_up_page/sign_up_screen.dart';
import 'package:flutterwithlaravel/page/splashscreen/splashscreen.dart';
import 'package:get/get.dart';

class AppRouts {
  static const String mainHomePageScreen = "/main_page_screen";
  static const String splashscreen = "/splashscreen";
  static const String homePage = "/home_page";
  static const String popularDetailScreen = "/popular_detail_screen";
  static const String recommendedDetailScreen = '/recommended_detail_screen';
  static const String cartScreen = '/cart_screen';
  static const String historyScreen = '/history_screen';
  static const String  signUpScreen ='/sign_up_screen';
  static const String signInScreen = '/sign_in_screen';

  static getPopularDetailScreen(int pageID, String page) => '$popularDetailScreen?pageID=$pageID&page=$page';
  static getRecommendedDetailScreen(int pageIDs,String page) => '$recommendedDetailScreen?pageID=$pageIDs&page=$page';

  static List<GetPage> pages = [
    GetPage(
      name: homePage,
      page: () => const HomePageScreen(),
      binding:HomePageBinding()
    ),
    GetPage(
      name: popularDetailScreen,
      page: () {
        var pageID = Get.parameters['pageID'];
        var page = Get.parameters['page'];
        print("page id is ${pageID}");
        return PopularFoodDetail(index: int.parse(pageID!), page: page.toString());
      },
      binding: PopularFoodDetailBinding(),
    ),
    GetPage(
      name: recommendedDetailScreen,
      page: () {
        var pageIDs = Get.parameters['pageID'];
        var page = Get.parameters['page'];
        print("page id is ${pageIDs}");
        return RecommendedFoodDetail(index: int.parse(pageIDs!), page: page.toString());
      },
      binding: RecommendedFoodBinding(),
    ),
    GetPage(
      name: cartScreen,
      page: () => const CartScreen(),
      binding: CartPageBinding(),
    ),
    GetPage(
      name: splashscreen,
      page: () => const SplashScreen(),
    ),
    GetPage(
      name: mainHomePageScreen,
      page: () => MainHomePage(),
      binding: HomePageBinding(),
    ),
    GetPage(
        name: historyScreen,
        page: ()=> const HistoryScreen(),
        binding: CartPageBinding(),
    ),
    GetPage(
        name:signUpScreen ,
        page: ()=> const SignUpScreen(),
        binding: SignUpBinding(),
    ),
    GetPage(
        name:signInScreen ,
        page: ()=> const SignInScreen()
    )
  ];
}
