
import 'package:dio/dio.dart';
import '../../constants/getx_constant/api_constant.dart';


class ApiClient{
  ApiClient._();
  static final instance = ApiClient._();
  static String token = ApiConstants.token;
  final Dio _dio = Dio(BaseOptions(
    baseUrl:ApiConstants.baseUrl,
    connectTimeout:  const Duration(seconds: 30),
    receiveTimeout: const Duration(seconds: 30),
    headers: {
      'Content-type':'application/json; charset=UTF-8',
      'Authorization':'Bearer $token'
    },
    responseType: ResponseType.json,
  ));

  Future<dynamic> get({
    required String path,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onReceiveProgress,
  }) async {
    try {
      final res = await _dio.get(
        path,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onReceiveProgress: onReceiveProgress,
      );
      if (res.statusCode == 200) {
        return res.data;
      }
      throw ("Opp Something went wrong with this get x client ");
    } catch (e) {
      rethrow;
    }
  }
  void updateToken(String tokens){
    token = tokens;
  }
  Future<dynamic> post({
    required String path,
    dynamic body,
  }) async{
    try{
      final res = await _dio.post(
        path,
        data: body,
        options: Options(
          headers: {
            'Content-type':'application/json; charset=UTF-8',
            'Authorization':'Bearer $token'
          },
        )
      );
      throw ("Opp Something went wrong with get x post");
    }catch(e){
      rethrow;
    }
  }
}