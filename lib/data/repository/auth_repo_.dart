
import 'package:dio/dio.dart';
import 'package:flutterwithlaravel/constants/getx_constant/api_constant.dart';
import 'package:flutterwithlaravel/constants/local_constant.dart';
import 'package:flutterwithlaravel/data/api_client/api_client.dart';
import 'package:flutterwithlaravel/model/sig_up_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthRepo {
  final SharedPreferences sp;
  AuthRepo({required this.sp});
  Future<Response> registration(SignUpModel signUpModel)async{
    final res = await ApiClient.instance.post(path: ApiConstants.authPath,body: signUpModel.toJson());
    return res;
  }
  saveUserToken(String token) async {
    ApiConstants.token = token;
    ApiClient.instance.updateToken(token);
    return await sp.setString(LocalConstant.token, token);
  }
}