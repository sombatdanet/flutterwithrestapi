import 'dart:convert';

import 'package:flutterwithlaravel/model/cart_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../constants/local_constant.dart';

class HistoryRepo {
  final SharedPreferences sp;
  HistoryRepo(this.sp);

   Future<bool> addHistory(List<CartModel> listHis) {
    final listOfHistory = getHistory();
    for (var element in listHis) {
      listOfHistory.add(element);
    }
     return addToListHistory(listOfHistory);
  }

  Future<bool> addToListHistory(List<CartModel> listHistory) async {
    List<String> listString = listHistory.map((e) => jsonEncode(e)).toList();
     return sp.setStringList(LocalConstant.historyKey, listString);
  }

  List<CartModel> getHistory() {
    final list = sp.getStringList(LocalConstant.historyKey);
    return list?.map((e) => CartModel.fromJson(jsonDecode(e))).toList() ?? [];
  }

  void clear() async {
    sp.remove(LocalConstant.historyKey);
  }
}
