import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutterwithlaravel/model/cart_model.dart';

import '../../constants/local_constant.dart';

class CartRep {
  final SharedPreferences sp;

  CartRep({required this.sp});
 Future<bool> addToCart(List<CartModel> lists) {
    final list =  getLocalListCartModel();
    for (var element in lists) {
      list.add(element);
    }
    return  addToCarts(list);
  }
  Future<bool> addToCarts (List<CartModel> cartModel) async{
    var time = DateTime.now().toString();
    for (var element in cartModel) {
      element.time = time;
    }
    List<String> cartList = cartModel.map((e) => jsonEncode(e)).toList();
    return sp.setStringList(LocalConstant.cartKey, cartList);
    }

  List<CartModel> getLocalListCartModel() {
    final cartList = sp.getStringList(LocalConstant.cartKey);
    return cartList?.map((e) => CartModel.fromJson(jsonDecode(e))).toList()??[];
  }
  Future<bool> removeCartLocal(){
    return sp.remove(LocalConstant.cartKey);
  }
}