import 'package:dio/dio.dart';
import 'package:flutterwithlaravel/Model/product_model.dart';
import 'package:flutterwithlaravel/constants/getx_constant/api_constant.dart';
import 'package:flutterwithlaravel/data/api_client/api_client.dart';

class RecommendedProductRepo{
  Future<List<Product>> getRecommendedProductList() async{
    try{
      final res = await ApiClient.instance.get(path:ApiConstants.recommendedPath);
      final recommendedProductsList = ProductModel.fromJson(res).products;
      return recommendedProductsList;
    }on DioException catch(e){
      throw e.message ?? "Opp Something went wrong with this repo ";
    }
  }
}