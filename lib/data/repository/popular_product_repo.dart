import 'package:dio/dio.dart';
import 'package:flutterwithlaravel/Model/product_model.dart';
import 'package:flutterwithlaravel/constants/getx_constant/api_constant.dart';
import 'package:flutterwithlaravel/data/api_client/api_client.dart';

class PopularProductRepo{

  Future<List<Product>> getPopularProductList() async{
    try{
      final res = await ApiClient.instance.get(path:ApiConstants.popularPath);
      final list = res;
      final popularProductList = ProductModel.fromJson(list).products;
      return popularProductList;
    }on DioException catch(e){
      throw e.message ?? "Opp Something went wrong with this repo ";
    }
  }
}